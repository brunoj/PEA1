//
// Created by brunoj on 23.10.17.
//

#ifndef DYNAMIC_DYNAMIC_H
#define DYNAMIC_DYNAMIC_H


#include <deque>
#include "Graph.h"
#include "Index.h"
#include <map>
#include <set>
#include <utility>

struct Class1Compare
{
    bool operator() (const Index& lhs, const Index& rhs) const
    {
//        if(lhs.vertexSet.size()!=rhs.vertexSet.size()){
//            if(rhs.vertexSet.size()<lhs.vertexSet.size())
//                return rhs.vertexSet.size()<lhs.vertexSet.size();
//        }else{
//            if(rhs.vertexSet==lhs.vertexSet){
//                return rhs.currentV<lhs.currentV;
//            }else{
//                return rhs.vertexSet<lhs.vertexSet;
//            }
//        }

        return std::tie (lhs.vertexSet, lhs.currentV) < std::tie (rhs.vertexSet, rhs.currentV);
    }
};
class Dynamic {
private:
    Graph *graph;
    int *visited;
public:
    Dynamic(Graph *graph);
    void GetMinRoute();
    int getCost(std::set<int> set, int prev, std::map<Index, int,Class1Compare> minCost);
    void print(std::map<Index, int,Class1Compare>,int vertNumber, int cost);
    long long GetMinRouteExp();

};


#endif //DYNAMIC_DYNAMIC_H
