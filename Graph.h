//
// Created by brunoj on 01.06.17.
//

#ifndef SDIZO3_GRAPH_H
#define SDIZO3_GRAPH_H
#include<iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iomanip>
#include <vector>

using namespace std;
class Graph {
private:
    int V;                                                               // V - ilosc wierzcholkow, E - ilosc krawedzi

public:
    Graph();
    ~Graph();
    vector<vector<int>> odleglosc;                                                                // Tablica wskaznikow przechowujaca wagi krawedzi miedzy wierzcholkami

    void setV(int V) { this -> V = V; };

    int getV() { return V; }

    void wyswietl();        // Funkcja wyswietlajaca graf w postaci macierzowej
    void generuj();
    void wczytaj(std::string a);
    void generujTEst( int ilosc_wierzcholkow);



};


#endif //SDIZO3_GRAPH_H
