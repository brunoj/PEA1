//
// Created by brunoj on 01.06.17.
//

#include "Graph.h"
#include <iostream>
#include <iomanip>
#include <values.h>
#include <limits.h>

using namespace std;

Graph::Graph():V(0) { }
Graph::~Graph() {
   odleglosc.clear();
}
void Graph::wyswietl() {
    // Jesli graf nie zawiera wierzcholkow
    if(getV() == 0) {
        cout << "Nie ma czego wyswietlic." << endl;
        return;
    }

    cout << "Postac macierzowa grafu: " << endl << endl;
    // Wyswietlenie wagi krawedzi wychodzacej z wierzcholka 'i' do wierzcholka 'j'nie m
    for(int i = 0; i < getV(); i++) {
        for(int j = 0; j < getV(); j++) {
            cout << setw(4) << odleglosc[i][j] << " ";
        }
        cout << endl << endl;
    }
}
void Graph::generuj() {
    int ilosc_wierzcholkow, gestosc, krawedzie, maksymalne_ilosc_krawedzi;
    cout << "Podaj liczbe miast: ";
    cin >> ilosc_wierzcholkow;

    // Jezeli podalismy za malo wierzcholkow
    if (ilosc_wierzcholkow < 2) {
        cout << "Graf musi posiadac co najmniej 2 miasta!" << endl;
        return;
    }
    setV(3);
    gestosc=100;


    // Obliczanie maks2ymalnej liczby krawedzi oraz zadanej liczby krawedzi (gestosc)
    maksymalne_ilosc_krawedzi = ilosc_wierzcholkow * ilosc_wierzcholkow - ilosc_wierzcholkow;
    krawedzie = 0.01 * gestosc * maksymalne_ilosc_krawedzi;





    if (getV() != 0) {
       odleglosc.clear();

    }
    setV(ilosc_wierzcholkow);
//    for (int i = 0; i < getV(); i++) G[i] = new int[getV()];
//    // Wyzerowanie wszystkich krawedzi
//    for (int i = 0; i < getV(); i++) { for (int j = 0; j < getV(); j++) G[i][j] = 0; }
    int waga;
    odleglosc.resize(getV());
    for(int i=0; i<ilosc_wierzcholkow;i++){
        odleglosc[i].resize(ilosc_wierzcholkow);
    }
    srand( time( NULL ) );
    for (int i = 0; i < ilosc_wierzcholkow; i++){

        for (int j = 0; j < ilosc_wierzcholkow; j++){

            if(i==j){
                odleglosc[i][j]=-1;
            }else{
                waga = (rand() % 49) + 1;
                odleglosc[i][j] = waga;
            }
        }
    }
}
void Graph::generujTEst(int ilosc_wierzcholkow){
    int  gestosc, krawedzie, maksymalne_ilosc_krawedzi;

    // Jezeli podalismy za malo wierzcholkow
    if (ilosc_wierzcholkow < 2) {
        cout << "Graf musi posiadac co najmniej 2 miasta!" << endl;
        return;
    }
    setV(3);
    gestosc=100;


    // Obliczanie maks2ymalnej liczby krawedzi oraz zadanej liczby krawedzi (gestosc)
    maksymalne_ilosc_krawedzi = ilosc_wierzcholkow * ilosc_wierzcholkow - ilosc_wierzcholkow;
    krawedzie = 0.01 * gestosc * maksymalne_ilosc_krawedzi;





    if (getV() != 0) {
        odleglosc.clear();

    }
    setV(ilosc_wierzcholkow);
//    for (int i = 0; i < getV(); i++) G[i] = new int[getV()];
//    // Wyzerowanie wszystkich krawedzi
//    for (int i = 0; i < getV(); i++) { for (int j = 0; j < getV(); j++) G[i][j] = 0; }
    int waga;
    odleglosc.resize(getV());
    for(int i=0; i<ilosc_wierzcholkow;i++){
        odleglosc[i].resize(ilosc_wierzcholkow);
    }
    srand( time( NULL ) );
    for (int i = 0; i < ilosc_wierzcholkow; i++){

        for (int j = 0; j < ilosc_wierzcholkow; j++){

            if(i==j){
                odleglosc[i][j]=INT_MAX;
            }else{
                waga = (rand() % 49) + 1;
                odleglosc[i][j] = waga;
            }
        }
    }
}
void Graph::wczytaj(std::string nazwa) {
    fstream wej;
    wej.open(nazwa);
    if(wej.good()){
        int ilosc_wierzcholkow;
        wej >>ilosc_wierzcholkow;
        setV(ilosc_wierzcholkow);
        odleglosc.resize(getV());
        for(int i=0; i<ilosc_wierzcholkow;i++){
            odleglosc[i].resize(ilosc_wierzcholkow);
        }
        for (int i = 0; i < ilosc_wierzcholkow; i++){

            for (int j = 0; j < ilosc_wierzcholkow; j++){
                wej >> odleglosc[i][j] ;
            }
        }
        wej.close();

    }
}
    //2
    //modyfikowanie do eulera
//
//
//void Graph::wczytaj(std::string nazwa) {
//    ifstream plik;
//    plik.open(nazwa);
//
//    if(!plik.good()) {
//        cout << "Nie ma takiego pliku." << endl;
//        return;
//    }
//
//    int licznik = 0, argument;
//    string zawartosc_wiersza, pomoc = "";
//    getline(plik, zawartosc_wiersza);
//
//    if(getV() != 0) {
//        for(int i = 0; i < getV(); i++) delete[] G[i];
//        delete[] G;
//    }
//
//    for(int i = 0; i < 4; i++) {
//        while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47) {
//            pomoc += zawartosc_wiersza[licznik];
//            licznik++;
//        }
//
//        argument = atoi(pomoc.c_str());
//        if(i == 0) { setE(argument); }
//        if(i == 1) { setV(argument); }
//        if(i == 2) { setPoczatek(argument); }
//        if(i == 3) { setKoniec(argument); }
//
//        licznik++;
//        pomoc = "";
//    }
//
//    G = new int*[getV()];
//    for(int i = 0; i < getV(); i++) G[i] = new int[getV()];
//    for(int i = 0; i < getV(); i++) { for(int j = 0; j < getV(); j++) G[i][j] = 0; }
//
//    licznik = 0;
//    int masymalna_ilosc_krawedzi = getV() * getV() - getV();
//    int wiersz, kolumna, waga;
//    for(int i = 0; i < masymalna_ilosc_krawedzi; i++) {
//        getline(plik, zawartosc_wiersza);
//        if(zawartosc_wiersza[0] > 57 || zawartosc_wiersza[0] < 48 || i == getE()) break;
//
//        for(int j = 0; j < 3; j++) {
//            while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47 || zawartosc_wiersza[licznik] == 45) {
//                pomoc += zawartosc_wiersza[licznik];
//                licznik++;
//            }
//
//            argument = atoi(pomoc.c_str());
//            if(j == 0) wiersz = argument;
//            if(j == 1) kolumna = argument;
//            if(j == 2) waga = argument;
//
//            licznik++;
//            pomoc = "";
//        }
//
//        licznik = 0;
//        G[wiersz][kolumna] = waga;
//        G[kolumna][wiersz] = waga;
//    }
//
//    plik.close();
//
//}