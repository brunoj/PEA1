//
// Created by brunoj on 23.10.17.
//

#include <vector>
#include <chrono>
#include <values.h>
#include <algorithm>    // std::find
#include <sstream>
#include <map>
#include <set>
#include <bitset>
#include "Dynamic.h"
#include <climits>
#include "Index.h"
#include <chrono>

Dynamic::Dynamic(Graph *graph) {
    this->graph = graph;
    this->visited = new int[this->graph->getV()];
    for (int i = 0; i < this->graph->getV(); i++) {
        this->visited[i] = 0;
    }
}
std::deque<std::set<int>> generate( int K){
    std::deque<std::set<int>> allSets;
    int helper = std::pow(2.00,K-1);
    std::string str = std::bitset<32>(helper - 1).to_string();
    str.erase(std::remove(str.begin(), str.end(), '0'), str.end());
    int size = str.size();
    int elements[size];
    for(int i=0;i<size;i++){
        elements[i]=i+1;
    }
    for(int i=0; i<helper-1;i++) {
        std::string str = std::bitset<32>(helper - 1-i).to_string();
        str.erase( 0, str.size()-size);
        std::set<int> tmp;
        for(int k=0; k<K; k++){
            //if the k-th bit is set
            if( (1<<k) & i){
                tmp.insert(elements[k]);
            }

        }


        allSets.push_back(tmp);
    }

    long a =allSets.size();
    int setsSize = (int) a;
//
//    for(int i=0;i<setsSize;i++)
//    {
//        std::set<int> tmp;
//        std::cout << "size: " << allSets.size() << "\n";
//        tmp = allSets.back();
//        allSets.pop_back();
//        for (int a : tmp) {
//            std::cout << a << ' ';
//        }
//
//    }
    return allSets;


}

void Dynamic::GetMinRoute(){
        auto start = chrono::system_clock::now();

    std::map<Index,int,Class1Compare> minCost;
        std::map<Index,int,Class1Compare> parent;
        deque<std::set<int>> listSets = generate( this->graph->getV() );
        //{} {1}.{2} ... {1,2}

        for(std::set<int> a: listSets){
            for(int current = 1; current < this->graph->getV(); current++){
                if(a.find(current) != a.end()){
                    continue;
                }
                Index index(a, current);
                int min = INT_MAX;
                int minPrev = 0;

                std::set<int> copy;
                copy = a;
                for(int prev : a){
                    int cost = this->graph->odleglosc[prev][current] + getCost(copy,prev,minCost) ;
                    if(cost<min){
                        min=cost;
                        minPrev=prev;
                    }
                }

                if(a.size()==0){
                    min = this->graph->odleglosc[0][current];
                }
                minCost.insert(std::pair<Index,int>(index,min));
                unsigned long a =  minCost.size();
                parent.insert(std::pair<Index,int>(index,minPrev));
            }
        }
    std::set<int> set;
    for(int i=1;i<this->graph->getV();i++){
        set.insert(i);
    }
    int min = INT_MAX;
    int prev = -1;
    string debug;
    std::set<int> copy;
    copy = set;
    for(int k : set){
        std::set<int> tmp;
        tmp = copy;
        tmp.erase(k);
        int cost = this->graph->odleglosc[k][0] + getCost(copy,k, minCost);
        if(cost<min){
            min=cost;
            prev = k;
        }
    }
    Index index(set,0);
    parent.insert(std::pair<Index,int>(index,prev));
    auto end = chrono::system_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
    std::cout << "Czas : " << elapsed.count() << endl;
    std::cout<<"cost "<<min;
    print(parent,this->graph->getV(),min);

}
void Dynamic::print(std::map<Index, int,Class1Compare> parent, int vertNumber, int cost) {
    std::set<int> set;
    for(int i=0; i<vertNumber;i++){
        set.insert(i);
    }
    int start = 0;
    std::deque<int> route;
    while(true){
        route.push_front(start);
        set.erase(start);
        Index index(set,start);
        if ( parent.find(index) == parent.end() ) {
           break;
        } else {
            start = parent.at(index);

        }

    }
    cout<<"cost == "<<cost<<"\n";
    for(int a: route){
        cout<<a<<" --> ";
    }
}
int Dynamic::getCost(std::set<int> set, int prev, std::map<Index, int,Class1Compare> minCost) {
    set.erase(prev);
    Index index(set,prev);
    if ( minCost.find(index) == minCost.end() ) {
        std::cout<<"jestem";
        return 0;
    } else {
        int cost = minCost.at(index);
        set.insert(prev);
        return cost;
    }


}

long long Dynamic::GetMinRouteExp(){
    auto start = chrono::system_clock::now();


        std::map<Index,int,Class1Compare> minCost;
        std::map<Index,int,Class1Compare> parent;
        deque<std::set<int>> listSets = generate( this->graph->getV() );
        //{} {1}.{2} ... {1,2}

        for(std::set<int> a: listSets){
            for(int current = 1; current < this->graph->getV(); current++){
                if(a.find(current) != a.end()){
                    continue;
                }
                Index index(a, current);
                int min = INT_MAX;
                int minPrev = 0;

                std::set<int> copy;
                copy = a;
                for(int prev : a){
                    int cost = this->graph->odleglosc[prev][current] + getCost(copy,prev,minCost) ;
                    if(cost<min){
                        min=cost;
                        minPrev=prev;
                    }
                }

                if(a.size()==0){
                    min = this->graph->odleglosc[0][current];
                }
                minCost.insert(std::pair<Index,int>(index,min));
                unsigned long a =  minCost.size();
                parent.insert(std::pair<Index,int>(index,minPrev));
            }
        }
        std::set<int> set;
        for(int i=1;i<this->graph->getV();i++){
            set.insert(i);
        }
        int min = INT_MAX;
        int prev = -1;
        string debug;
        std::set<int> copy;
        copy = set;
        for(int k : set){
            std::set<int> tmp;
            tmp = copy;
            tmp.erase(k);
            int cost = this->graph->odleglosc[k][0] + getCost(copy,k, minCost);
            if(cost<min){
                min=cost;
                prev = k;
            }
        }
        Index index(set,0);
        parent.insert(std::pair<Index,int>(index,prev));



    auto end = chrono::system_clock::now();
    auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
    return elapsed.count();
}
