#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <bitset>
#include <vector>
#include <algorithm>
#include <cstring>
#include <set>
#include <deque>
#include <iterator>
#include "Index.h"
#include "Graph.h"
#include "Dynamic.h"

void comb(int K)
{
    std::deque<std::set<int>> allSets;
    int helper = std::pow(2.00,K);
    std::string str = std::bitset<32>(helper - 1).to_string();
    str.erase(std::remove(str.begin(), str.end(), '0'), str.end());
    int size = str.size();
    int elements[size];
    for(int i=0;i<size;i++){
        elements[i]=i+1;
    }
    for(int i=0; i<helper-1;i++) {
        std::string str = std::bitset<32>(helper - 1-i).to_string();
        str.erase( 0, str.size()-size);
        std::set<int> tmp;
        for(int k=0; k<3; k++){
            //if the k-th bit is set
            if( (1<<k) & i){
                tmp.insert(elements[k]);
            }

        }


            allSets.push_back(tmp);
    }
    long a =allSets.size();
    int setsSize = (int) a;

    for(int i=0;i<setsSize;i++)
    {
            std::set<int> tmp;
            std::cout << "size: " << allSets.size() << " ";
            tmp = allSets.back();
            allSets.pop_back();
            for (int a : tmp) {
                std::cout << a << ' ';
            }
                std::cout<<"\n";
    }


}


int main()
{
    Graph *graph = new Graph;
    cout<<"Prgramowanie Dynamiczne Dla problemu komiwojażera\n";
    cout<<"Generuj graf losowo        ---   1\n";
    cout<<"Wczytaj graf  z pliku      ---   2\n";
    int wybor;
    cin>>wybor;
    switch(wybor){
        case 1: {
            int wybor1;
            cout << "Test         ---- 1\n";
            cout << "eksperyment  ---- 2\n";
            cin >> wybor1;
            cout << "\n";
            if (wybor1 == 1) {
                graph->generuj();
                graph->wyswietl();
                Dynamic cDynamic(graph);
                cDynamic.GetMinRoute();
            } else if (wybor1 == 2) {
                cout << "ile razy powtorzyc?\n";

                int howmany;
                cin >> howmany;
                cout << "\n";
                cout << "ile Miast?\n";
                int city;
                cin>>city;
                cout<<"\n";
                long long helper = 0;
                for(int i=0;i<howmany;i++){
                    graph->generujTEst(city);
                    Dynamic aDynamic(graph);

                    helper = helper + aDynamic.GetMinRouteExp();
                }
                cout<<"srtedni czas to: "<<helper/howmany<<"\n";
            } else {
                cout << "zly wybor";
                return 0;
            }
            break;
        }
        case 2: {
            string nazwa;
            cout << "podaj sciezke do pliku\n";
            cin >> nazwa;
            cout << "\n";
            graph->wczytaj(nazwa);
            graph->wyswietl();

            Dynamic bDynamic(graph);
            bDynamic.GetMinRoute();
        }
        default:
            cout<<"zly wybor";
            return 0;

    }




}
