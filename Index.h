//
// Created by brunoj on 11.11.17.
//

#ifndef DYNAMIC_INDEX_H
#define DYNAMIC_INDEX_H

#include <set>

class Index {
private:
    static int i;
public:

    std::set<int> vertexSet;
    int currentV;
    int cor;
    Index(std::set<int> vertexSet, int v);
};


#endif //DYNAMIC_INDEX_H
